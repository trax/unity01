﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

	public GameObject[] floorTiles;
	public GameObject[] wallTiles;
	public GameObject cornerTile;
	public GameObject Ninja;

	
	private int lines, columns;
	void BoardSetup (int _lines, int _columns) {
		lines = _lines;
		columns = _columns;


		// board 
		for (int y = 0; y < lines; y++) {
			for (int x = 0 ; x < columns ; x++){
				if(x == 2 && y == 2){
					Instantiate(Ninja, new Vector3(x, y, 0f), Quaternion.identity);
					continue;
				}
				GameObject floorTile = floorTiles [Random.Range(0, floorTiles.Length)];
				Instantiate(floorTile, new Vector3(x, y, 0f), Quaternion.identity);
			}
		}

		//border
		for (int y = 0; y < lines ; y++) {
			Debug.Log("instr");
			Instantiate(wallTiles[Random.Range(0, wallTiles.Length)], new Vector3(-1, y, 0f), Quaternion.Euler (0,0,270));
			Instantiate(wallTiles[Random.Range(0, wallTiles.Length)], new Vector3(columns, y, 0f), Quaternion.Euler (0,0,90));
		}
		for (int x = 0; x < columns; x++) {
			Instantiate(wallTiles[Random.Range(0, wallTiles.Length)], new Vector3(x, -1, 0f), Quaternion.identity);
			Instantiate(wallTiles[Random.Range(0, wallTiles.Length)], new Vector3(x, lines, 0f), Quaternion.Euler (0,0,180));
		}
		Instantiate(cornerTile, new Vector3(-1, -1, 0f), Quaternion.Euler (0,0,180));
		Instantiate(cornerTile, new Vector3(-1, lines, 0f), Quaternion.Euler (0,0,90));
		Instantiate(cornerTile, new Vector3(columns, -1, 0f), Quaternion.Euler (0,0,270));
		Instantiate(cornerTile, new Vector3(columns, lines, 0f), Quaternion.identity);


		
	}


	void Start (){
	// Update is called once per frame	void Update () {
		BoardSetup (5,6);
	}
}
